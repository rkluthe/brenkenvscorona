# BrenkenVsCorona

Im Rahmen der Corona Pandemie haben Brenkener 3D Drucker Besitzer entschieden, ehrenamtlich Personen mit sogenannten Face Shields auszustatten. 

Die kleine Brenkener Gruppe arbeitet mit dem bundesweit gegründeten Netzwerk "MakerVsVirus" zusammen. Eine direkte Zusammenarbeit besteht in diesem 
Zusammenhang mit dem Paderborn-Höxter C3PB MakerVsVirus Hub. Damit ist die Brenkener Gruppe in der Lage, Nachfrage nach Schilden über den Hub zu bedienen, 
die ihre eingenen Kapazitäten überschreiten. 

Die Schilde werden kostenlos abgegeben. Um die Kosten für Material und evtl. Ersatzteile für Drucker zu decken, sind die Brenkener Gruppe und der Hub auf 
Spenden angewiesen. Der Verein C3PB ist gemeinnützig und kann demzufolge Spendenquittungen ausstellen.



In diesem Gitlab Projekt werden alle Unterlagen der Brenkener Gruppe von den verwendeten Druckvorlagen für 3D-Drucker, Lochvorlagen für Folien bis hin zu Dokumentationen 
öffentlich und freiverfügbar bereitsgestellt.


   *Dokumentation:* Dinge wie Beipackzettel, Montageanleitungen, etc.

   *Modelle:* Druckdaten für deinen 3D-Drucker


Du wohnst in der Region Büren, hast einen 3D-Drucker und möchtest mithelfen? Melde dich bitte zur Abstimmung auf *corona@connectdevices.de*.

Du vertrittst eine Klinik, Praxis, Schule,o.ä. oder hast einfach nur privaten Bedarf melde dich bitte über die Email *corona@connectdevices.de*
